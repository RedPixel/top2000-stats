import glob
import openpyxl
from pathlib import Path
from collections import defaultdict

xlsx_files = [path for path in Path('.').rglob('*.xlsx')]
wbs = [openpyxl.load_workbook(wb) for wb in xlsx_files]

counts = defaultdict(int)
for wb in wbs:
    sheet = wb.active
    for row in sheet.rows:
        title = row[1].value
        artist = row[2].value
        song = artist + " - " + title
        counts[song] += 1

persistant_song = [k for k, v in counts.items() if v == 21]
for song in persistant_song:
    print(song)
print(len(persistant_song))